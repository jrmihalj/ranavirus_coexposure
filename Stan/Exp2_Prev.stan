data{
  int N_obs;
  int<lower=0> Infect[N_obs];
  int<lower=1,upper=2> Time[N_obs];
  int<lower=1,upper=2> Treat[N_obs];
}

parameters{
  
  // Fixed effects:
  real beta_Time[2];
  real beta_Treat[2];
  real beta_Inter;

}

transformed parameters{
  real lp_infect[N_obs];
  real<lower = 0, upper = 1> p_infect[N_obs];
  
  
  for(i in 1:N_obs){
    
    lp_infect[i] = 
    0 +
    beta_Treat[Treat[i]] +
    beta_Time[Time[i]] +
    beta_Inter * Time[i] * Treat[i];
    
    p_infect[i] = inv_logit(lp_infect[i]);
    
  }
}

model{
  
  // Priors:
  beta_Time ~ normal(0,10);
  beta_Treat ~ normal(0,10);
  beta_Inter ~ normal(0,10);
  
  // Likelihood:
  
  Infect ~ binomial(12, p_infect);
  
}
