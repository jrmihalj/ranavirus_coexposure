data{
  int N_obs;
  
  real<lower=0, upper=1> Double[N_obs];

  real<lower=0, upper=1> ATV[N_obs];
  real<lower=0, upper=1> FV3[N_obs];
  real<lower=0, upper=1> RFV3[N_obs];
  
  int<lower=0> Infect[N_obs];
}

parameters{
  
  // Fixed effects:
  real beta_atv;
  real beta_fv3;
  real beta_rfv3;

  // Double effects
  real beta_datv;
  real beta_dfv3;
  real beta_drfv3;

  // Coexposure effects (interactions)
  real beta_af;
  real beta_ar;
  real beta_fr;
}

transformed parameters{
  real lp_infect[N_obs];
  real<lower = 0, upper = 1> p_infect[N_obs];
  
  
  for(i in 1:N_obs){
    lp_infect[i] = 
    //Intercept: so that it starts near zero
    //logit(1e-10) + 
    0 +
    //Strain fixed effects (essentially effect of single dose)
    beta_atv*ATV[i] + beta_fv3*FV3[i] + beta_rfv3*RFV3[i] + 
    //Double dosage effects
    beta_datv*Double[i]*ATV[i] + beta_dfv3*Double[i]*FV3[i] + beta_drfv3*Double[i]*RFV3[i] + 
    //Coexposure effects (essentially interactions)
    beta_af*ATV[i]*FV3[i] + beta_ar*ATV[i]*RFV3[i] + beta_fr*FV3[i]*RFV3[i];
    
    p_infect[i] = inv_logit(lp_infect[i]);
  }
}

model{
  
  // Priors:
  beta_atv ~ normal(0,10);
  beta_fv3 ~ normal(0,10);
  beta_rfv3 ~ normal(0,10);

  beta_datv ~ normal(0,10);
  beta_dfv3 ~ normal(0,10);
  beta_drfv3 ~ normal(0,10);

  beta_af ~ normal(0,10);
  beta_ar ~ normal(0,10);
  beta_fr ~ normal(0,10);
  
  // Likelihood:
  
  Infect ~ binomial(25, p_infect);
  
}
