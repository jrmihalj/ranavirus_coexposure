data{
  int N_obs;
  int N_rep;
  real Load[N_obs];
  int<lower=1,upper=2> Time[N_obs];
  //real Time[N_obs];
  int<lower=1,upper=2> Treat[N_obs];
  real<lower=0,upper=1> Died[N_obs];
  int<lower=1,upper=N_rep> Rep[N_obs];
}

parameters{
  
  // Intercept:
  real alpha;
  
  // Fixed effects:
  real beta_Time[2];
  //real beta_Time;
  real beta_Treat[2];
  real beta_Inter;
  real beta_Died;
  
  // Random effects:
  real eta[N_rep];
  real<lower = 0> sigma_rep;
  
  // Residuals:
  real<lower = 0> sigma_resid;

}

transformed parameters{
  real mu[N_obs];
  
  for(i in 1:N_obs){
    
    mu[i] = 
    alpha +
    beta_Treat[Treat[i]] +
    beta_Time[Time[i]] +
    //beta_Time * Time[i] +
    beta_Inter * Time[i] * Treat[i] +
    beta_Died * Died[i] +
    eta[Rep[i]];
    
  }
}

model{
  
  // Priors:
  alpha ~ normal(5,5);
  
  beta_Time ~ normal(0,5);
  beta_Treat ~ normal(0,5);
  beta_Inter ~ normal(0,5);
  beta_Died ~ normal(0,5);
  
  eta ~ normal(0, sigma_rep);
  sigma_rep ~ cauchy(0,2.5);
  sigma_resid ~ cauchy(0,2.5);
  
  
  // Likelihood:
  
  Load ~ normal(mu, sigma_resid);
  
}
