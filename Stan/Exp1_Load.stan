data{
  int N_obs;
  
  real<lower=0, upper=1> Double[N_obs];

  real<lower=0, upper=1> ATV[N_obs];
  real<lower=0, upper=1> FV3[N_obs];
  real<lower=0, upper=1> RFV3[N_obs];
  real<lower=0, upper=1> Died[N_obs];
  
  real Load[N_obs];
}

parameters{
  
  // Fixed effects:
  real alpha;
  real<lower=0> sigma;
  
  //real beta_atv;
  real beta_fv3;
  real beta_rfv3;

  // Double effects
  //real beta_datv;
  real beta_dfv3;
  real beta_drfv3;

  // Coexposure effects (interactions)
  real beta_af;
  real beta_ar;
  real beta_fr;
  
  //Death effect:
  real beta_died;
}

transformed parameters{
  real mu_load[N_obs];
  //real<lower = 0, upper = 1> p_infect[N_obs];
  
  
  for(i in 1:N_obs){
    mu_load[i] = 
    //Intercept: so that it starts near zero
    alpha +
    //Strain fixed effects (essentially effect of single dose)
    beta_fv3*FV3[i] + beta_rfv3*RFV3[i] + 
    //Double dosage effects
    beta_dfv3*Double[i]*FV3[i] + beta_drfv3*Double[i]*RFV3[i] + 
    //Coexposure effects (essentially interactions)
    beta_af*ATV[i]*FV3[i] + beta_ar*ATV[i]*RFV3[i] + beta_fr*FV3[i]*RFV3[i] +
    //Effect of death
    beta_died * Died[i];
  }
  
}

model{
  
  // Priors:
  alpha ~ normal(5,5);
  beta_fv3 ~ normal(0,5);
  beta_rfv3 ~ normal(0,5);

  beta_dfv3 ~ normal(0,5);
  beta_drfv3 ~ normal(0,5);

  beta_af ~ normal(0,5);
  beta_ar ~ normal(0,5);
  beta_fr ~ normal(0,5);
  
  beta_died ~ normal(0,5);
  
  sigma ~ cauchy(0,2.5);
  
  // Likelihood:
  
  Load ~ normal(mu_load, sigma);
  
}
