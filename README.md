---
title: "README"
output: html_document
---


This is the README file for:   
Co-exposure to multiple *Ranavirus* types enhances viral infectivity and replication in a larval amphibian system   
Mihaljevic et al. (in review)   

---

The directory "./R" contains all of the R code for running the analyses in the paper, and for generating the figures. 

---

The directory "./Stan" contains all of the Stan model files necessary to run the Bayesian analyses, which are run via the R scripts, above.

---

The directory "./data" contains all of the .csv files necessary to run the R scripts. It also contains .RData files, which are the specific results and posterior samples used to create the Tables in the manuscript. These are included because every time the Bayesian models are run, new posterior samples will be generated, which will change the calculation of 95% credible interval very slightly. 
